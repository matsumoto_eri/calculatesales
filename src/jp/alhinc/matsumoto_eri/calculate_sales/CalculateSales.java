package jp.alhinc.matsumoto_eri.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;


public class CalculateSales {

	public static void main(String[] args) {

		//処理内容1
		BufferedReader br = null;  //1行単位でファイルからデータを読み取る初期値nullでセット
		Map<String, String> branchMap = new TreeMap<>();
		Map<String, Long> saleMap = new TreeMap<>();  //for文内に入れるとmapが新しくなってしまうので外

		try {
			File file = new File(args[0], "branch.lst");
			br = new BufferedReader(new FileReader(file));

			//エラー処理3-1.1
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}



			//支店コードと支店名をMapを使ってキーと値として保持
			String line;
			while((line = br.readLine()) != null) {   //()内が実行され、lineがnullでなければ実行

				//branch.listをカンマで分割
				String[] data = line.split(",");

				//エラー処理1-2
				//分割した文字列が2つ、数字が3桁、2つ目に何か入っている
				if(data.length != 2 || !data[0].matches("[0-9]{3}") || data[1].isEmpty()){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}


				//mapに支店名義ファイルの1つ目をキーとして、2つ目を値とした
				branchMap.put(data[0], data[1]);
				//salemapに最初に支店定義ファイルの1つ目を、2つ目を0として準備
				saleMap.put(data[0], 0L);


			}//while


			//処理内容2
			File dir = new File(args[0]);  //ファイル指定
			File[] list = dir.listFiles(); //一覧取得



			//連番チェック 売上ファイルを格納するList
			ArrayList<File> saleFileList = new ArrayList<>();
			ArrayList<Integer> nums = new ArrayList<>();


			if(list != null) {
				for(int i = 0; i < list.length; i++) { //1つずつ見ていく

					String fileName = list[i].getName();  //ファイルの名前をString型で定義


					//数字8桁の.rcdのファイルを検索してsaleFileListに格納
					if(fileName.matches("^[0-9]{8}.rcd")){
						saleFileList.add(new File(fileName));  //ArrayListへ格納
						String FileNumberName = fileName.substring(0, 8);  //8桁を切り出す
						nums.add(Integer.parseInt(FileNumberName));  //数値化

						//売上ファイルを読み込んでいく
						BufferedReader br2 = null;
						br2 = new BufferedReader(new FileReader(list[i]));

						try {
							//一行ずつ読みこんで変数を割り当てる
							String code = br2.readLine();
							String value = br2.readLine();
							String s = br2.readLine();


							if(!branchMap.containsKey(code)) {
								System.out.println(fileName + "の支店コードが不正です");
								return;
							}
							//エラー処理2-4
							if(s != null) {
								System.out.println(fileName + "のフォーマットが不正です");
								return;
							}

							Long values = Long.parseLong(value); //2行目を文字列から整数に
							Long branchTotal = values + saleMap.get(code);

							//エラー処理2-2
							if(String.valueOf(branchTotal).length() > 10) {
								System.out.println("合計金額が10桁を超えました");
								return;
							}

							saleMap.put(code, branchTotal);  //あれば加算して戻す

						} finally {
							br2.close();  //ファイルを閉じる
						}
					}
				}
			}

			//連番チェック用

			Collections.sort(nums);  //Listの中を昇順に並べ替え

			//連番チェック用 最小値と最大値を使って比較
			//連番なら最小値に要素数を足すと最大値の-1になることを利用
			int min = nums.get(0);
			int max = nums.get(nums.size() - 1);
			if(max != min + nums.size() -1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}



			//処理内容3
			File fileOut = new File(args[0], "branch.out");
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileOut));

			try {
				for(Map.Entry<String, String> entry : branchMap.entrySet()) {
					bw.write(entry.getKey() + "," + entry.getValue() + "," + saleMap.get(entry.getKey()));
					bw.newLine();
				}
			} finally {
				bw.close();   //ファイルを閉じる
			}


		} catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;

		} finally {
			try {
				if(br != null) {
					br.close();
				}
			} catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
			}

		}
	}
}







